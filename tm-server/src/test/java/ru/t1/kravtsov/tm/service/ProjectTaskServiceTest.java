package ru.t1.kravtsov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.service.IProjectTaskService;
import ru.t1.kravtsov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kravtsov.tm.exception.entity.TaskNotFoundException;
import ru.t1.kravtsov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kravtsov.tm.exception.field.TaskIdEmptyException;
import ru.t1.kravtsov.tm.exception.field.UserIdEmptyException;
import ru.t1.kravtsov.tm.marker.UnitCategory;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.repository.ProjectRepository;
import ru.t1.kravtsov.tm.repository.TaskRepository;

import java.util.Arrays;
import java.util.List;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final Task alphaTask = new Task();

    @NotNull
    private final Task betaTask = new Task();

    @NotNull
    private final Project alphaProject = new Project();

    @Before
    public void before() {
        alphaProject.setId("alpha-project-id");
        alphaProject.setName("alpha");
        alphaProject.setUserId("user1");
        alphaTask.setId("alpha-task-id");
        alphaTask.setName("alpha");
        alphaTask.setId("alpha-task-id");
        alphaTask.setProjectId(alphaProject.getId());
        alphaTask.setUserId("user1");
        betaTask.setId("beta-task-id");
        betaTask.setName("beta");
        betaTask.setUserId("user1");
        taskRepository.add(alphaTask);
        taskRepository.add(betaTask);
        projectRepository.add(alphaProject);
    }

    @After
    public void after() {
        projectRepository.clear();
        taskRepository.clear();
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertNotEquals(alphaProject.getId(), betaTask.getProjectId());
        @NotNull final Task task = service.bindTaskToProject(alphaProject.getUserId(), alphaProject.getId(), betaTask.getId());
        Assert.assertSame(betaTask, task);
        Assert.assertEquals(alphaProject.getId(), betaTask.getProjectId());

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject(null, alphaProject.getId(), betaTask.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.bindTaskToProject("", alphaProject.getId(), betaTask.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(alphaProject.getUserId(), null, betaTask.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.bindTaskToProject(alphaProject.getUserId(), "", betaTask.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(alphaProject.getUserId(), alphaProject.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.bindTaskToProject(alphaProject.getUserId(), alphaProject.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.bindTaskToProject("not-existing-project-id", alphaProject.getId(), betaTask.getId());
        });
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertEquals(alphaProject.getId(), alphaTask.getProjectId());
        @NotNull final Task task = service.unbindTaskFromProject(alphaProject.getUserId(), alphaProject.getId(), alphaTask.getId());
        Assert.assertSame(alphaTask, task);
        Assert.assertNull(alphaTask.getProjectId());

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject(null, alphaProject.getId(), betaTask.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.unbindTaskFromProject("", alphaProject.getId(), betaTask.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(alphaProject.getUserId(), null, betaTask.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.unbindTaskFromProject(alphaProject.getUserId(), "", betaTask.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(alphaProject.getUserId(), alphaProject.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            service.unbindTaskFromProject(alphaProject.getUserId(), alphaProject.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.unbindTaskFromProject("not-existing-project-id", alphaProject.getId(), betaTask.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.unbindTaskFromProject(alphaProject.getUserId(), alphaProject.getId(), "not-existing-task-id");
        });
    }

    @Test
    public void removeProjectById() {
        service.removeProjectById(alphaProject.getUserId(), alphaProject.getId());
        Assert.assertFalse(projectRepository.existsById(alphaProject.getUserId()));
    }

    @Test
    public void removeProjectByIdWithBadArgs() {
        @NotNull final List<Project> initialProjects = Arrays.asList(alphaProject);
        service.removeProjectById(null, alphaProject.getId());
        Assert.assertEquals(initialProjects, projectRepository.findAll());
        service.removeProjectById("", alphaProject.getId());
        Assert.assertEquals(initialProjects, projectRepository.findAll());
        service.removeProjectById(alphaProject.getUserId(), "");
        Assert.assertEquals(initialProjects, projectRepository.findAll());
        service.removeProjectById(alphaProject.getUserId(), null);
        Assert.assertEquals(initialProjects, projectRepository.findAll());
        service.removeProjectById(alphaProject.getUserId(), "not-existing-project-id");
        Assert.assertEquals(initialProjects, projectRepository.findAll());
    }

    @Test
    public void removeProjects() {
        service.removeProjects(alphaProject.getUserId(), Arrays.asList(alphaProject));
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void removeProjectsWithBadArgs() {
        @NotNull final List<Project> initialProjects = Arrays.asList(alphaProject);
        service.removeProjects(null, Arrays.asList(alphaProject));
        service.removeProjects("", Arrays.asList(alphaProject));
        final Project nullProject = null;
        service.removeProjects("", Arrays.asList(nullProject));
        Assert.assertEquals(initialProjects, projectRepository.findAll());
    }

}
