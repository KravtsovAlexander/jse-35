package ru.t1.kravtsov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kravtsov.tm.api.repository.IUserRepository;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.marker.UnitCategory;
import ru.t1.kravtsov.tm.model.User;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final User alphaUser = new User();

    @NotNull
    private final User betaUser = new User();

    @NotNull
    private final User gammaUser = new User();

    {
        alphaUser.setId("alpha-user-id");
        alphaUser.setLogin("alpha");
        alphaUser.setEmail("alpha@user.com");
        betaUser.setId("beta-user-id");
        betaUser.setRole(Role.ADMIN);
        betaUser.setLogin("beta");
        betaUser.setEmail("beta@user.com");
        gammaUser.setId("gamma-user-id");
        gammaUser.setLogin("gamma");
        gammaUser.setEmail("gamma@user.com");
    }

    @Before
    public void before() {
        repository.add(alphaUser);
        repository.add(betaUser);
        repository.add(gammaUser);
    }

    @After
    public void after() {
        repository.clear();
    }

    @Test
    public void add() {
        final int initialSize = repository.getSize();
        Assert.assertNotNull(repository.add(new User()));
        Assert.assertEquals(initialSize + 1, repository.getSize());
    }

    @Test
    public void addCollection() {
        final int initialSize = repository.getSize();
        @NotNull final List<User> users = Arrays.asList(new User(), new User());
        Assert.assertNotNull(repository.add(users));
        Assert.assertEquals(initialSize + users.size(), repository.getSize());
    }

    @Test
    public void clear() {
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void deleteAll() {
        repository.deleteAll();
        Assert.assertEquals(0, repository.getSize());

    }

    @Test
    public void deleteAllSpecificUsers() {
        @NotNull final List<User> users = Arrays.asList(alphaUser, betaUser);
        repository.deleteAll(users);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertSame(gammaUser, repository.findAll().get(0));
    }

    @Test
    public void findAll() {
        @NotNull List<User> users = repository.findAll();
        @NotNull List<User> expected = Arrays.asList(alphaUser, betaUser, gammaUser);
        Assert.assertEquals(expected, users);

        repository.clear();
        Assert.assertNotNull(repository.findAll());
    }

    @Test
    public void findAllWithComparator() {
        repository.clear();
        repository.add(betaUser);
        repository.add(gammaUser);
        repository.add(alphaUser);

        @NotNull final Comparator<User> comparator = (o1, o2) -> {
            if (o1 == null || o2 == null) return 0;
            return o1.getLogin().compareTo(o2.getLogin());
        };
        @NotNull List<User> users = repository.findAll(comparator);
        @NotNull List<User> expected = Arrays.asList(alphaUser, betaUser, gammaUser);
        Assert.assertEquals(expected, users);
    }

    @Test
    public void set() {
        @NotNull final List<User> users = Arrays.asList(new User(), new User());
        Assert.assertNotNull(repository.set(users));
        Assert.assertEquals(users, repository.findAll());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(repository.existsById(alphaUser.getId()));
        Assert.assertFalse(repository.existsById("not-existing-user-id"));
    }

    @Test
    public void findOneById() {
        Assert.assertSame(alphaUser, repository.findOneById(alphaUser.getId()));
        Assert.assertNull(repository.findOneById("not-existing-user-id"));
    }

    @Test
    public void findOneByIndex() {
        Assert.assertSame(betaUser, repository.findOneByIndex(1));
        Assert.assertNull(repository.findOneByIndex(999));
    }

    @Test
    public void getSize() {
        Assert.assertEquals(3, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void remove() {
        @Nullable final User removedUser = repository.remove(alphaUser);
        Assert.assertSame(alphaUser, removedUser);
        Assert.assertFalse(repository.existsById(alphaUser.getId()));

        Assert.assertNull(repository.remove(null));
    }

    @Test
    public void removeById() {
        @Nullable final User removedUser = repository.removeById(alphaUser.getId());
        Assert.assertSame(alphaUser, removedUser);
        Assert.assertFalse(repository.existsById(alphaUser.getId()));

        Assert.assertNull(repository.removeById("not-existing-user-id"));
    }

    @Test
    public void removeByIndex() {
        @Nullable final User removedUser = repository.removeByIndex(0);
        Assert.assertSame(alphaUser, removedUser);
        Assert.assertFalse(repository.existsById(alphaUser.getId()));

        Assert.assertNull(repository.removeByIndex(999));
    }

    @Test
    public void findByLogin() {
        @Nullable User user = repository.findByLogin(alphaUser.getLogin());
        Assert.assertSame(alphaUser, user);

        user = repository.findByLogin("not-existing-user");
        Assert.assertNull(user);
    }

    @Test
    public void findByEmail() {
        @Nullable User user = repository.findByEmail(alphaUser.getEmail());
        Assert.assertSame(alphaUser, user);

        user = repository.findByEmail("not-existing-user");
        Assert.assertNull(user);
    }

    @Test
    public void doesLoginExist() {
        Assert.assertTrue(repository.doesLoginExist(alphaUser.getLogin()));
        Assert.assertFalse(repository.doesLoginExist("not-existing-user-login"));
    }

    @Test
    public void doesEmailExist() {
        Assert.assertTrue(repository.doesEmailExist(alphaUser.getEmail()));
        Assert.assertFalse(repository.doesEmailExist("not-existing-user-email"));
    }

}
