package ru.t1.kravtsov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.service.ITaskService;
import ru.t1.kravtsov.tm.comparator.NameComparator;
import ru.t1.kravtsov.tm.comparator.StatusComparator;
import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.exception.entity.EntityNotFoundException;
import ru.t1.kravtsov.tm.exception.entity.TaskNotFoundException;
import ru.t1.kravtsov.tm.exception.field.*;
import ru.t1.kravtsov.tm.marker.UnitCategory;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.repository.TaskRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @NotNull
    private final Task alphaTask = new Task("alpha", "testTask");

    @NotNull
    private final Task betaTask = new Task("beta", "testTask");

    @NotNull
    private final Task gammaTask = new Task("gamma", "testTask");

    {
        alphaTask.setStatus(Status.COMPLETED);
        alphaTask.setUserId("user1");
        alphaTask.setId("alpha-task-id");
        alphaTask.setProjectId("project1");
        betaTask.setStatus(Status.IN_PROGRESS);
        betaTask.setUserId("user2");
        betaTask.setId("beta-task-id");
        betaTask.setProjectId("project1");
        gammaTask.setStatus(Status.NOT_STARTED);
        gammaTask.setUserId("user1");
        gammaTask.setId("gamma-task-id");
        gammaTask.setProjectId("project2");
    }

    @Before
    public void before() {
        repository.add(alphaTask);
        repository.add(betaTask);
        repository.add(gammaTask);
    }

    @After
    public void after() {
        repository.clear();
    }

    @Test
    public void create() {
        @Nullable final Task task = service.create("user1", "delta");
        Assert.assertNotEquals(0, repository.getSize());
        Assert.assertEquals("delta", task.getName());
        Assert.assertEquals("user1", task.getUserId());
        Assert.assertNotNull(task.getId());
        Assert.assertTrue(repository.existsById(task.getId()));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, "epsilon");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", "epsilon");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create("user1", null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create("user1", "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, "epsilon", "desc");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", "epsilon", "desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create("user1", null, "desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create("user1", "", "desc");
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create("user1", "test", null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create("user1", "test", "");
        });
    }

    @Test
    public void createWithDescription() {
        @Nullable final Task task = service.create("user1", "delta", "desc");
        Assert.assertNotEquals(0, repository.getSize());
        Assert.assertEquals("delta", task.getName());
        Assert.assertEquals("user1", task.getUserId());
        Assert.assertEquals("desc", task.getDescription());
        Assert.assertNotNull(task.getId());
        Assert.assertTrue(repository.existsById(task.getId()));
    }

    @Test
    public void updateById() {
        @NotNull final Task task = new Task();
        task.setId("test-task");
        task.setName("old name");
        task.setDescription("old desc");
        task.setUserId("user3");
        repository.add(task);

        service.updateById(task.getUserId(), task.getId(), "new name", "new desc");
        Assert.assertEquals("new name", task.getName());
        Assert.assertEquals("new desc", task.getDescription());

        service.updateById(task.getUserId(), task.getId(), "new name", null);
        Assert.assertEquals("", task.getDescription());

        repository.remove(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, task.getId(), "new name", "new desc");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", task.getId(), "new name", "new desc");
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(task.getUserId(), null, "new name", "new desc");
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(task.getUserId(), "", "new name", "new desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(task.getUserId(), task.getId(), null, "new desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(task.getUserId(), task.getId(), "", "new desc");
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.updateById(task.getUserId(), task.getId(), "new name", "new desc");
        });

    }

    @Test
    public void updateByIndex() {
        @NotNull final Task task = new Task();
        task.setId("test-task");
        task.setName("old name");
        task.setDescription("old desc");
        task.setUserId("user3");
        repository.add(task);
        int index = 0;

        service.updateByIndex(task.getUserId(), index, "new name", "new desc");
        Assert.assertEquals("new name", task.getName());
        Assert.assertEquals("new desc", task.getDescription());

        service.updateByIndex(task.getUserId(), index, "new name", null);
        Assert.assertEquals("", task.getDescription());

        repository.remove(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex(null, index, "new name", "new desc");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex("", index, "new name", "new desc");
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(task.getUserId(), null, "new name", "new desc");
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(task.getUserId(), -1, "new name", "new desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(task.getUserId(), index, null, "new desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(task.getUserId(), index, "", "new desc");
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.updateByIndex(task.getUserId(), index, "new name", "new desc");
        });
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Task task = new Task();
        task.setId("test-task");
        task.setName("old name");
        task.setDescription("old desc");
        task.setUserId("user3");
        repository.add(task);

        service.changeTaskStatusById(task.getUserId(), task.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());

        repository.remove(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById(null, task.getId(), Status.IN_PROGRESS);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusById("", task.getId(), Status.IN_PROGRESS);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(task.getUserId(), null, Status.IN_PROGRESS);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeTaskStatusById(task.getUserId(), "", Status.IN_PROGRESS);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeTaskStatusById(task.getUserId(), task.getId(), null);
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.changeTaskStatusById(task.getUserId(), task.getId(), Status.IN_PROGRESS);
        });
    }

    @Test
    public void changeTaskStatusByIndex() {
        @NotNull final Task task = new Task();
        task.setId("test-task");
        task.setName("old name");
        task.setDescription("old desc");
        task.setUserId("user3");
        repository.add(task);
        int index = 0;

        service.changeTaskStatusByIndex(task.getUserId(), index, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());

        repository.remove(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusByIndex(null, index, Status.IN_PROGRESS);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeTaskStatusByIndex("", index, Status.IN_PROGRESS);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeTaskStatusByIndex(task.getUserId(), null, Status.IN_PROGRESS);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeTaskStatusByIndex(task.getUserId(), -1, Status.IN_PROGRESS);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeTaskStatusByIndex(task.getUserId(), index, null);
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            service.changeTaskStatusByIndex(task.getUserId(), index, Status.IN_PROGRESS);
        });
    }

    @Test
    public void removeByName() {
        @NotNull final List<Task> tasks = service.removeByName(alphaTask.getUserId(), alphaTask.getName());
        Assert.assertEquals(1, tasks.size());
        Assert.assertSame(alphaTask, tasks.get(0));
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.findOneById(alphaTask.getId());
        });

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByName(null, alphaTask.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByName("", alphaTask.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.removeByName(alphaTask.getUserId(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.removeByName(alphaTask.getUserId(), "");
        });
    }

    @Test
    public void add() {
        final int initialSize = service.getSize();
        Assert.assertNotNull(service.add(new Task()));
        Assert.assertEquals(initialSize + 1, service.getSize());

        Assert.assertThrows(EntityNotFoundException.class, () -> {
            final Task task = null;
            service.add(task);
        });
    }

    @Test
    public void addCollection() {
        int initialSize = service.getSize();
        @NotNull List<Task> tasks = Arrays.asList(new Task(), new Task());
        Assert.assertNotNull(service.add(tasks));
        Assert.assertEquals(initialSize + tasks.size(), service.getSize());

        initialSize = service.getSize();
        tasks = Collections.emptyList();
        Assert.assertEquals(Collections.emptyList(), service.add(tasks));
        Assert.assertEquals(initialSize, service.getSize());
    }

    @Test
    public void addForUser() {
        final int initialSize = service.getSize();
        @NotNull final Task task = new Task();
        Assert.assertNotNull(service.add("user1", task));
        Assert.assertEquals(initialSize + 1, service.getSize());
        Assert.assertEquals("user1", task.getUserId());

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, new Task());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add("", new Task());
        });
        Assert.assertNull(service.add("user1", null));
    }

    @Test
    public void clear() {
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void clearForUser() {
        service.clear("user1");
        Assert.assertEquals(1, service.getSize());
        Assert.assertSame(betaTask, service.findAll().get(0));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
    }

    @Test
    public void deleteAll() {
        service.deleteAll();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void deleteAllSpecificTasks() {
        @NotNull final List<Task> tasks = Arrays.asList(alphaTask, betaTask);
        service.deleteAll(tasks);
        Assert.assertEquals(1, service.getSize());
        Assert.assertSame(gammaTask, service.findAll().get(0));
    }

    @Test
    public void deleteAllUserTasks() {
        service.deleteAll("user1");
        Assert.assertEquals(1, service.getSize());
        Assert.assertSame(betaTask, service.findAll().get(0));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable final String nullId = null;
            service.deleteAll(nullId);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.deleteAll("");
        });
    }

    @Test
    public void findAll() {
        @NotNull List<Task> tasks = service.findAll();
        @NotNull List<Task> expected = Arrays.asList(alphaTask, betaTask, gammaTask);
        Assert.assertEquals(expected, tasks);

        service.clear();
        Assert.assertNotNull(service.findAll());
    }

    @Test
    public void findAllUserTasks() {
        @NotNull List<Task> tasks = service.findAll("user1");
        @NotNull List<Task> expected = Arrays.asList(alphaTask, gammaTask);
        Assert.assertEquals(expected, tasks);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            final String nullId = null;
            service.findAll(nullId);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
    }

    @Test
    public void findAllWithComparator() {
        service.clear();
        service.add(betaTask);
        service.add(gammaTask);
        service.add(alphaTask);

        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        @NotNull List<Task> tasks = service.findAll(comparator);
        @NotNull List<Task> expected = Arrays.asList(alphaTask, betaTask, gammaTask);
        Assert.assertEquals(expected, tasks);
    }

    @Test
    public void findAllWithNullComparator() {
        service.clear();
        service.add(betaTask);
        service.add(gammaTask);
        service.add(alphaTask);

        @NotNull final Comparator comparator = null;
        @NotNull List<Task> tasks = service.findAll(comparator);
        @NotNull List<Task> expected = Arrays.asList(betaTask, gammaTask, alphaTask);
        Assert.assertEquals(expected, tasks);
    }

    @Test
    public void findAllUserTasksWithComparator() {
        @NotNull final Comparator comparator = StatusComparator.INSTANCE;
        @NotNull List<Task> tasks = service.findAll("user1", comparator);
        @NotNull List<Task> expected = Arrays.asList(gammaTask, alphaTask);
        Assert.assertEquals(expected, tasks);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("", comparator);
        });
    }

    @Test
    public void findAllUserTasksWithNullComparator() {
        @NotNull final Comparator comparator = null;
        @NotNull List<Task> tasks = service.findAll("user1", comparator);
        @NotNull List<Task> expected = Arrays.asList(alphaTask, gammaTask);
        Assert.assertEquals(expected, tasks);
    }

    @Test
    public void findAllTasksWithSort() {
        @NotNull List<Task> tasks = service.findAll(Sort.BY_STATUS);
        @NotNull List<Task> expected = Arrays.asList(gammaTask, betaTask, alphaTask);
        Assert.assertEquals(expected, tasks);

        final Sort sort = null;
        tasks = service.findAll(sort);
        expected = Arrays.asList(alphaTask, betaTask, gammaTask);
        Assert.assertEquals(expected, tasks);
    }

    @Test
    public void findAllUserTasksWithSort() {
        @NotNull List<Task> tasks = service.findAll("user1", Sort.BY_STATUS);
        @NotNull List<Task> expected = Arrays.asList(gammaTask, alphaTask);
        Assert.assertEquals(expected, tasks);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll(null, Sort.BY_STATUS);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("", Sort.BY_STATUS);
        });
    }

    @Test
    public void findAllUserTasksWithNullSort() {
        final Sort sort = null;
        @NotNull List<Task> tasks = service.findAll("user1", sort);
        @NotNull List<Task> expected = Arrays.asList(alphaTask, gammaTask);
        Assert.assertEquals(expected, tasks);
    }

    @Test
    public void set() {
        @NotNull final List<Task> tasks = Arrays.asList(new Task(), new Task());
        Assert.assertNotNull(service.set(tasks));
        Assert.assertEquals(tasks, service.findAll());

        Assert.assertEquals(Collections.emptyList(), service.set(Collections.emptyList()));
        Assert.assertEquals(tasks, service.findAll());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(service.existsById(alphaTask.getId()));
        Assert.assertFalse(service.existsById("not-existing-task-id"));
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById("");
        });
    }

    @Test
    public void existsUserTaskById() {
        Assert.assertTrue(service.existsById("user1", alphaTask.getId()));
        Assert.assertFalse(service.existsById("user2", alphaTask.getId()));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, betaTask.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", betaTask.getId());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById("user1", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById("user1", "");
        });
    }

    @Test
    public void findOneById() {
        Assert.assertSame(alphaTask, service.findOneById(alphaTask.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.findOneById("not-existing-task-id");
        });
    }

    @Test
    public void findOneUserTaskById() {
        Assert.assertSame(alphaTask, service.findOneById("user1", alphaTask.getId()));
        Assert.assertNull(service.findOneById("user2", alphaTask.getId()));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneById(null, alphaTask.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneById("", alphaTask.getId());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("user1", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("user1", "");
        });
    }

    @Test
    public void findOneByIndex() {
        Assert.assertSame(betaTask, service.findOneByIndex(1));
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(-1);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.findOneByIndex(999);
        });
    }

    @Test
    public void findOneUserTaskByIndex() {
        Assert.assertSame(betaTask, service.findOneByIndex("user2", 0));
        Assert.assertNull(service.findOneByIndex("user2", 1));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, 0);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", 0);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex("user1", null);
        });
    }

    @Test
    public void getSize() {
        Assert.assertEquals(3, service.getSize());
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void getSizeOfUserTasks() {
        Assert.assertEquals(2, service.getSize("user1"));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize("");
        });
    }

    @Test
    public void remove() {
        @Nullable final Task removedTask = service.remove(alphaTask);
        Assert.assertSame(alphaTask, removedTask);
        Assert.assertFalse(service.existsById(alphaTask.getId()));

        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.remove(null);
        });
    }

    @Test
    public void removeUserTask() {
        @Nullable Task removedTask = service.remove("user1", alphaTask);
        Assert.assertSame(alphaTask, removedTask);
        Assert.assertFalse(service.existsById(alphaTask.getId()));

        removedTask = service.remove("user1", betaTask);
        Assert.assertNull(removedTask);
        Assert.assertTrue(service.existsById(betaTask.getId()));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, gammaTask);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", gammaTask);
        });
    }

    @Test
    public void removeUserNullTask() {
        final Task task = null;
        Assert.assertNull(service.remove("user1", task));
    }

    @Test
    public void removeById() {
        @Nullable final Task removedTask = service.removeById(alphaTask.getId());
        Assert.assertSame(alphaTask, removedTask);
        Assert.assertFalse(service.existsById(alphaTask.getId()));

        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.removeById("not-existing-task-id");
        });
    }

    @Test
    public void removeUserTaskById() {
        @Nullable Task removedTask = service.removeById("user1", alphaTask.getId());
        Assert.assertSame(alphaTask, removedTask);
        Assert.assertFalse(service.existsById(alphaTask.getId()));

        removedTask = service.removeById("user1", betaTask.getId());
        Assert.assertNull(removedTask);
        Assert.assertTrue(service.existsById(betaTask.getId()));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, gammaTask.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", gammaTask.getId());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("user2", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("user2", "");
        });
    }

    @Test
    public void removeByIndex() {
        @Nullable final Task removedTask = service.removeByIndex(0);
        Assert.assertSame(alphaTask, removedTask);
        Assert.assertFalse(service.existsById(alphaTask.getId()));

        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(-1);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.removeByIndex(999);
        });
    }

    @Test
    public void removeUserTaskByIndex() {
        @Nullable Task removedTask = service.removeByIndex("user1", 1);
        Assert.assertSame(gammaTask, removedTask);
        Assert.assertFalse(service.existsById(gammaTask.getId()));

        removedTask = service.removeByIndex("user2", 1);
        Assert.assertNull(removedTask);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex(null, 0);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex("", 0);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex("user2", null);
        });
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final List<Task> tasks = service.findAllByProjectId("user1", "project1");
        @NotNull final List<Task> expected = Arrays.asList(alphaTask);
        Assert.assertEquals(expected, tasks);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAllByProjectId("", "project1");
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            service.findAllByProjectId("user1", "");
        });
    }

}
