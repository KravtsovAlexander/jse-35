package ru.t1.kravtsov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.service.IProjectService;
import ru.t1.kravtsov.tm.comparator.NameComparator;
import ru.t1.kravtsov.tm.comparator.StatusComparator;
import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.exception.entity.EntityNotFoundException;
import ru.t1.kravtsov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kravtsov.tm.exception.field.*;
import ru.t1.kravtsov.tm.marker.UnitCategory;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.repository.ProjectRepository;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IProjectService service = new ProjectService(repository);

    @NotNull
    private final Project alphaProject = new Project("alpha", "testProject");

    @NotNull
    private final Project betaProject = new Project("beta", "testProject");

    @NotNull
    private final Project gammaProject = new Project("gamma", "testProject");

    {
        alphaProject.setStatus(Status.COMPLETED);
        alphaProject.setUserId("user1");
        alphaProject.setId("alpha-project-id");
        betaProject.setStatus(Status.IN_PROGRESS);
        betaProject.setUserId("user2");
        betaProject.setId("beta-project-id");
        gammaProject.setStatus(Status.NOT_STARTED);
        gammaProject.setUserId("user1");
        gammaProject.setId("gamma-project-id");
    }

    @Before
    public void before() {
        repository.add(alphaProject);
        repository.add(betaProject);
        repository.add(gammaProject);
    }

    @After
    public void after() {
        repository.clear();
    }

    @Test
    public void create() {
        @Nullable final Project project = service.create("user1", "delta");
        Assert.assertNotEquals(0, repository.getSize());
        Assert.assertEquals("delta", project.getName());
        Assert.assertEquals("user1", project.getUserId());
        Assert.assertNotNull(project.getId());
        Assert.assertTrue(repository.existsById(project.getId()));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, "epsilon");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", "epsilon");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create("user1", null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create("user1", "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create(null, "epsilon", "desc");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.create("", "epsilon", "desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create("user1", null, "desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.create("user1", "", "desc");
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create("user1", "test", null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            service.create("user1", "test", "");
        });
    }

    @Test
    public void createWithDescription() {
        @Nullable final Project project = service.create("user1", "delta", "desc");
        Assert.assertNotEquals(0, repository.getSize());
        Assert.assertEquals("delta", project.getName());
        Assert.assertEquals("user1", project.getUserId());
        Assert.assertEquals("desc", project.getDescription());
        Assert.assertNotNull(project.getId());
        Assert.assertTrue(repository.existsById(project.getId()));
    }

    @Test
    public void updateById() {
        @NotNull final Project project = new Project();
        project.setId("test-project");
        project.setName("old name");
        project.setDescription("old desc");
        project.setUserId("user3");
        repository.add(project);

        service.updateById(project.getUserId(), project.getId(), "new name", "new desc");
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new desc", project.getDescription());

        service.updateById(project.getUserId(), project.getId(), "new name", null);
        Assert.assertEquals("", project.getDescription());

        repository.remove(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById(null, project.getId(), "new name", "new desc");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateById("", project.getId(), "new name", "new desc");
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(project.getUserId(), null, "new name", "new desc");
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateById(project.getUserId(), "", "new name", "new desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(project.getUserId(), project.getId(), null, "new desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateById(project.getUserId(), project.getId(), "", "new desc");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.updateById(project.getUserId(), project.getId(), "new name", "new desc");
        });

    }

    @Test
    public void updateByIndex() {
        @NotNull final Project project = new Project();
        project.setId("test-project");
        project.setName("old name");
        project.setDescription("old desc");
        project.setUserId("user3");
        repository.add(project);
        int index = 0;

        service.updateByIndex(project.getUserId(), index, "new name", "new desc");
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new desc", project.getDescription());

        service.updateByIndex(project.getUserId(), index, "new name", null);
        Assert.assertEquals("", project.getDescription());

        repository.remove(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex(null, index, "new name", "new desc");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.updateByIndex("", index, "new name", "new desc");
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(project.getUserId(), null, "new name", "new desc");
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.updateByIndex(project.getUserId(), -1, "new name", "new desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(project.getUserId(), index, null, "new desc");
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.updateByIndex(project.getUserId(), index, "", "new desc");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.updateByIndex(project.getUserId(), index, "new name", "new desc");
        });
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Project project = new Project();
        project.setId("test-project");
        project.setName("old name");
        project.setDescription("old desc");
        project.setUserId("user3");
        repository.add(project);

        service.changeProjectStatusById(project.getUserId(), project.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());

        repository.remove(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById(null, project.getId(), Status.IN_PROGRESS);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusById("", project.getId(), Status.IN_PROGRESS);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(project.getUserId(), null, Status.IN_PROGRESS);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.changeProjectStatusById(project.getUserId(), "", Status.IN_PROGRESS);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeProjectStatusById(project.getUserId(), project.getId(), null);
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.changeProjectStatusById(project.getUserId(), project.getId(), Status.IN_PROGRESS);
        });
    }

    @Test
    public void changeProjectStatusByIndex() {
        @NotNull final Project project = new Project();
        project.setId("test-project");
        project.setName("old name");
        project.setDescription("old desc");
        project.setUserId("user3");
        repository.add(project);
        int index = 0;

        service.changeProjectStatusByIndex(project.getUserId(), index, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());

        repository.remove(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusByIndex(null, index, Status.IN_PROGRESS);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.changeProjectStatusByIndex("", index, Status.IN_PROGRESS);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeProjectStatusByIndex(project.getUserId(), null, Status.IN_PROGRESS);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.changeProjectStatusByIndex(project.getUserId(), -1, Status.IN_PROGRESS);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            service.changeProjectStatusByIndex(project.getUserId(), index, null);
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            service.changeProjectStatusByIndex(project.getUserId(), index, Status.IN_PROGRESS);
        });
    }

    @Test
    public void removeByName() {
        @NotNull final List<Project> projects = service.removeByName(alphaProject.getUserId(), alphaProject.getName());
        Assert.assertEquals(1, projects.size());
        Assert.assertSame(alphaProject, projects.get(0));
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.findOneById(alphaProject.getId());
        });

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByName(null, alphaProject.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByName("", alphaProject.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.removeByName(alphaProject.getUserId(), null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            service.removeByName(alphaProject.getUserId(), "");
        });
    }

    @Test
    public void add() {
        final int initialSize = service.getSize();
        Assert.assertNotNull(service.add(new Project()));
        Assert.assertEquals(initialSize + 1, service.getSize());

        Assert.assertThrows(EntityNotFoundException.class, () -> {
            final Project project = null;
            service.add(project);
        });
    }

    @Test
    public void addCollection() {
        int initialSize = service.getSize();
        @NotNull List<Project> projects = Arrays.asList(new Project(), new Project());
        Assert.assertNotNull(service.add(projects));
        Assert.assertEquals(initialSize + projects.size(), service.getSize());

        initialSize = service.getSize();
        projects = Collections.emptyList();
        Assert.assertEquals(Collections.emptyList(), service.add(projects));
        Assert.assertEquals(initialSize, service.getSize());
    }

    @Test
    public void addForUser() {
        final int initialSize = service.getSize();
        @NotNull final Project project = new Project();
        Assert.assertNotNull(service.add("user1", project));
        Assert.assertEquals(initialSize + 1, service.getSize());
        Assert.assertEquals("user1", project.getUserId());

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add(null, new Project());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.add("", new Project());
        });
        Assert.assertNull(service.add("user1", null));
    }

    @Test
    public void clear() {
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void clearForUser() {
        service.clear("user1");
        Assert.assertEquals(1, service.getSize());
        Assert.assertSame(betaProject, service.findAll().get(0));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.clear("");
        });
    }

    @Test
    public void deleteAll() {
        service.deleteAll();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void deleteAllSpecificProjects() {
        @NotNull final List<Project> projects = Arrays.asList(alphaProject, betaProject);
        service.deleteAll(projects);
        Assert.assertEquals(1, service.getSize());
        Assert.assertSame(gammaProject, service.findAll().get(0));
    }

    @Test
    public void deleteAllUserProjects() {
        service.deleteAll("user1");
        Assert.assertEquals(1, service.getSize());
        Assert.assertSame(betaProject, service.findAll().get(0));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable final String nullId = null;
            service.deleteAll(nullId);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.deleteAll("");
        });
    }

    @Test
    public void findAll() {
        @NotNull List<Project> projects = service.findAll();
        @NotNull List<Project> expected = Arrays.asList(alphaProject, betaProject, gammaProject);
        Assert.assertEquals(expected, projects);

        service.clear();
        Assert.assertNotNull(service.findAll());
    }

    @Test
    public void findAllUserProjects() {
        @NotNull List<Project> projects = service.findAll("user1");
        @NotNull List<Project> expected = Arrays.asList(alphaProject, gammaProject);
        Assert.assertEquals(expected, projects);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            final String nullId = null;
            service.findAll(nullId);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("");
        });
    }

    @Test
    public void findAllWithComparator() {
        service.clear();
        service.add(betaProject);
        service.add(gammaProject);
        service.add(alphaProject);

        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        @NotNull List<Project> projects = service.findAll(comparator);
        @NotNull List<Project> expected = Arrays.asList(alphaProject, betaProject, gammaProject);
        Assert.assertEquals(expected, projects);
    }

    @Test
    public void findAllWithNullComparator() {
        service.clear();
        service.add(betaProject);
        service.add(gammaProject);
        service.add(alphaProject);

        @NotNull final Comparator comparator = null;
        @NotNull List<Project> projects = service.findAll(comparator);
        @NotNull List<Project> expected = Arrays.asList(betaProject, gammaProject, alphaProject);
        Assert.assertEquals(expected, projects);
    }

    @Test
    public void findAllUserProjectsWithComparator() {
        @NotNull final Comparator comparator = StatusComparator.INSTANCE;
        @NotNull List<Project> projects = service.findAll("user1", comparator);
        @NotNull List<Project> expected = Arrays.asList(gammaProject, alphaProject);
        Assert.assertEquals(expected, projects);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("", comparator);
        });
    }

    @Test
    public void findAllUserProjectsWithNullComparator() {
        @NotNull final Comparator comparator = null;
        @NotNull List<Project> projects = service.findAll("user1", comparator);
        @NotNull List<Project> expected = Arrays.asList(alphaProject, gammaProject);
        Assert.assertEquals(expected, projects);
    }

    @Test
    public void findAllProjectsWithSort() {
        @NotNull List<Project> projects = service.findAll(Sort.BY_STATUS);
        @NotNull List<Project> expected = Arrays.asList(gammaProject, betaProject, alphaProject);
        Assert.assertEquals(expected, projects);

        final Sort sort = null;
        projects = service.findAll(sort);
        expected = Arrays.asList(alphaProject, betaProject, gammaProject);
        Assert.assertEquals(expected, projects);
    }

    @Test
    public void findAllUserProjectsWithSort() {
        @NotNull List<Project> projects = service.findAll("user1", Sort.BY_STATUS);
        @NotNull List<Project> expected = Arrays.asList(gammaProject, alphaProject);
        Assert.assertEquals(expected, projects);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll(null, Sort.BY_STATUS);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findAll("", Sort.BY_STATUS);
        });
    }

    @Test
    public void findAllUserProjectsWithNullSort() {
        final Sort sort = null;
        @NotNull List<Project> projects = service.findAll("user1", sort);
        @NotNull List<Project> expected = Arrays.asList(alphaProject, gammaProject);
        Assert.assertEquals(expected, projects);
    }

    @Test
    public void set() {
        @NotNull final List<Project> projects = Arrays.asList(new Project(), new Project());
        Assert.assertNotNull(service.set(projects));
        Assert.assertEquals(projects, service.findAll());

        Assert.assertEquals(Collections.emptyList(), service.set(Collections.emptyList()));
        Assert.assertEquals(projects, service.findAll());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(service.existsById(alphaProject.getId()));
        Assert.assertFalse(service.existsById("not-existing-project-id"));
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById("");
        });
    }

    @Test
    public void existsUserProjectById() {
        Assert.assertTrue(service.existsById("user1", alphaProject.getId()));
        Assert.assertFalse(service.existsById("user2", alphaProject.getId()));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById(null, betaProject.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.existsById("", betaProject.getId());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById("user1", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.existsById("user1", "");
        });
    }

    @Test
    public void findOneById() {
        Assert.assertSame(alphaProject, service.findOneById(alphaProject.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.findOneById("not-existing-project-id");
        });
    }

    @Test
    public void findOneUserProjectById() {
        Assert.assertSame(alphaProject, service.findOneById("user1", alphaProject.getId()));
        Assert.assertNull(service.findOneById("user2", alphaProject.getId()));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneById(null, alphaProject.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneById("", alphaProject.getId());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("user1", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.findOneById("user1", "");
        });
    }

    @Test
    public void findOneByIndex() {
        Assert.assertSame(betaProject, service.findOneByIndex(1));
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex(-1);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.findOneByIndex(999);
        });
    }

    @Test
    public void findOneUserProjectByIndex() {
        Assert.assertSame(betaProject, service.findOneByIndex("user2", 0));
        Assert.assertNull(service.findOneByIndex("user2", 1));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex(null, 0);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.findOneByIndex("", 0);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.findOneByIndex("user1", null);
        });
    }

    @Test
    public void getSize() {
        Assert.assertEquals(3, service.getSize());
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void getSizeOfUserProjects() {
        Assert.assertEquals(2, service.getSize("user1"));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize(null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.getSize("");
        });
    }

    @Test
    public void remove() {
        @Nullable final Project removedProject = service.remove(alphaProject);
        Assert.assertSame(alphaProject, removedProject);
        Assert.assertFalse(service.existsById(alphaProject.getId()));

        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.remove(null);
        });
    }

    @Test
    public void removeUserProject() {
        @Nullable Project removedProject = service.remove("user1", alphaProject);
        Assert.assertSame(alphaProject, removedProject);
        Assert.assertFalse(service.existsById(alphaProject.getId()));

        removedProject = service.remove("user1", betaProject);
        Assert.assertNull(removedProject);
        Assert.assertTrue(service.existsById(betaProject.getId()));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove(null, gammaProject);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.remove("", gammaProject);
        });
    }

    @Test
    public void removeUserNullProject() {
        final Project project = null;
        Assert.assertNull(service.remove("user1", project));
    }

    @Test
    public void removeById() {
        @Nullable final Project removedProject = service.removeById(alphaProject.getId());
        Assert.assertSame(alphaProject, removedProject);
        Assert.assertFalse(service.existsById(alphaProject.getId()));

        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("");
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.removeById("not-existing-project-id");
        });
    }

    @Test
    public void removeUserProjectById() {
        @Nullable Project removedProject = service.removeById("user1", alphaProject.getId());
        Assert.assertSame(alphaProject, removedProject);
        Assert.assertFalse(service.existsById(alphaProject.getId()));

        removedProject = service.removeById("user1", betaProject.getId());
        Assert.assertNull(removedProject);
        Assert.assertTrue(service.existsById(betaProject.getId()));

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById(null, gammaProject.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeById("", gammaProject.getId());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("user2", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.removeById("user2", "");
        });
    }

    @Test
    public void removeByIndex() {
        @Nullable final Project removedProject = service.removeByIndex(0);
        Assert.assertSame(alphaProject, removedProject);
        Assert.assertFalse(service.existsById(alphaProject.getId()));

        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex(-1);
        });
        Assert.assertThrows(EntityNotFoundException.class, () -> {
            service.removeByIndex(999);
        });
    }

    @Test
    public void removeUserProjectByIndex() {
        @Nullable Project removedProject = service.removeByIndex("user1", 1);
        Assert.assertSame(gammaProject, removedProject);
        Assert.assertFalse(service.existsById(gammaProject.getId()));

        removedProject = service.removeByIndex("user2", 1);
        Assert.assertNull(removedProject);

        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex(null, 0);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            service.removeByIndex("", 0);
        });
        Assert.assertThrows(IndexIncorrectException.class, () -> {
            service.removeByIndex("user2", null);
        });
    }

}
