package ru.t1.kravtsov.tm.repository;

import com.hazelcast.core.HazelcastInstance;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IUserRepository;
import ru.t1.kravtsov.tm.model.User;

import java.util.function.Predicate;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(final @NotNull HazelcastInstance instance) {
        super(instance);
    }

    @NotNull
    private Predicate<User> filterByLogin(@NotNull final String login) {
        return m -> login.equals(m.getLogin());
    }

    @NotNull
    private Predicate<User> filterByEmail(@NotNull final String email) {
        return m -> email.equals(m.getEmail());
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return findAll()
                .stream()
                .filter(filterByLogin(login))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return findAll()
                .stream()
                .filter(filterByEmail(email))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean doesLoginExist(@NotNull final String login) {
        return findAll()
                .stream()
                .anyMatch(filterByLogin(login));
    }

    @NotNull
    @Override
    public Boolean doesEmailExist(@NotNull final String email) {
        return findAll()
                .stream()
                .anyMatch(filterByEmail(email));
    }

}
