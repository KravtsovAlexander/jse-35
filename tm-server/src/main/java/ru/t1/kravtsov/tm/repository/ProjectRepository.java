package ru.t1.kravtsov.tm.repository;

import com.hazelcast.core.HazelcastInstance;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.model.Project;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(final @NotNull HazelcastInstance instance) {
        super(instance);
    }

    @NotNull
    @Override
    public List<Project> removeByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId)
                .stream()
                .filter(m -> name.equals(m.getName()))
                .peek(m -> models.remove(m.getId()))
                .collect(Collectors.toList());
    }

}
