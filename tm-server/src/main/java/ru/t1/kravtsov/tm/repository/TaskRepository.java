package ru.t1.kravtsov.tm.repository;

import com.hazelcast.core.HazelcastInstance;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(final @NotNull HazelcastInstance instance) {
        super(instance);
    }

    @NotNull
    @Override
    public List<Task> removeByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId)
                .stream()
                .filter(m -> name.equals(m.getName()))
                .peek(m -> models.remove(m.getId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return findAll(userId)
                .stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

}
