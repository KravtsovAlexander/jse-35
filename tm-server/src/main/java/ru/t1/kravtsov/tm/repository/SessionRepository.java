package ru.t1.kravtsov.tm.repository;

import com.hazelcast.core.HazelcastInstance;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.repository.ISessionRepository;
import ru.t1.kravtsov.tm.model.Session;

@NoArgsConstructor
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(final @NotNull HazelcastInstance instance) {
        super(instance);
    }

}
