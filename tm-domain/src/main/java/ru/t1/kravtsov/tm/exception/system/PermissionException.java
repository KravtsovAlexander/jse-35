package ru.t1.kravtsov.tm.exception.system;

public final class PermissionException extends AbstractSystemException {

    public PermissionException() {
        super("Error. Permissions error.");
    }

}
