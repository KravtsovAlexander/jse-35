package ru.t1.kravtsov.tm.dto.response;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.User;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private User user;

    public AbstractUserResponse(@Nullable final User user) {
        this.user = user;
    }

}
