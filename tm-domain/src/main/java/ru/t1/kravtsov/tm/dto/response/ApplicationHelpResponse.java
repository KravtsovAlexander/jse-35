package ru.t1.kravtsov.tm.dto.response;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
public class ApplicationHelpResponse extends AbstractResponse {

    @Nullable
    private List<String> commands;

}
