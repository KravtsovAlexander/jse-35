package ru.t1.kravtsov.tm.dto.response;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
public class TaskListResponse extends AbstractTaskResponse {

    @Nullable
    private List<Task> tasks;

    public TaskListResponse(@Nullable List<Task> tasks) {
        this.tasks = tasks;
    }

}
