package ru.t1.kravtsov.tm.api.endpoint;

import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.dto.request.*;
import ru.t1.kravtsov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@WebService
@Path("/api/ProjectEndpoint")
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @POST
    @NotNull
    @WebMethod
    @Path("/changeProjectStatusById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectChangeStatusByIdResponse changeProjectStatusById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/changeProjectStatusByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIndexRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/clearProject")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectClearResponse clearProject(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/completeProjectById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectCompleteByIdResponse completeProjectById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/completeProjectByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectCompleteByIndexResponse completeProjectByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIndexRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/createProject")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectCreateResponse createProject(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/displayProjectById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectDisplayByIdResponse displayProjectById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectDisplayByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/displayProjectByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectDisplayByIndexResponse displayProjectByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectDisplayByIndexRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/listProject")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectListResponse listProject(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/removeProjectById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectRemoveByIdResponse removeProjectById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/removeProjectByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectRemoveByIndexResponse removeProjectByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/removeProjectByName")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectRemoveByNameResponse removeProjectByName(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByNameRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/startProjectById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectStartByIdResponse startProjectById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/startProjectByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectStartByIndexResponse startProjectByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIndexRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/updateProjectById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectUpdateByIdResponse updateProjectById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/updateProjectByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ProjectUpdateByIndexResponse updateProjectByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    );

}
