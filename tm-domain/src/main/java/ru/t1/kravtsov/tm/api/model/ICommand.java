package ru.t1.kravtsov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ICommand {

    void execute();

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @NotNull
    String getName();

}
