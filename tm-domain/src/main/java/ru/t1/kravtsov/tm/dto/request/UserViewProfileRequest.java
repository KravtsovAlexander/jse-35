package ru.t1.kravtsov.tm.dto.request;

import io.swagger.annotations.ApiModel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@ApiModel
@NoArgsConstructor
public class UserViewProfileRequest extends AbstractUserRequest {

    public UserViewProfileRequest(final @Nullable String token) {
        super(token);
    }

}
