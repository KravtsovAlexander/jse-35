package ru.t1.kravtsov.tm.dto.request;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
public class TaskDisplayByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskDisplayByIdRequest(final @Nullable String token) {
        super(token);
    }

}
