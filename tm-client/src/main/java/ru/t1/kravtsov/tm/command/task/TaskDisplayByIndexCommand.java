package ru.t1.kravtsov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.dto.request.TaskDisplayByIndexRequest;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskDisplayByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Display task by index.";

    @NotNull
    public static final String NAME = "task-display-by-index";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer input = TerminalUtil.nextNumber();
        @NotNull final Integer index = input - 1;
        @NotNull final TaskDisplayByIndexRequest request = new TaskDisplayByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final Task task = getTaskEndpoint()
                .displayTaskByIndex(request)
                .getTask();
        displayTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
