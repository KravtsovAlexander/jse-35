package ru.t1.kravtsov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.dto.request.UserLogoutRequest;
import ru.t1.kravtsov.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Logout.";

    @NotNull
    public static final String NAME = "logout";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
        setToken(null);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
