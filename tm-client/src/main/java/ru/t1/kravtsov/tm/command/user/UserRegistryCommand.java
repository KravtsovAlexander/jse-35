package ru.t1.kravtsov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.dto.request.UserRegistryRequest;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.model.User;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Registry user.";

    @NotNull
    public static final String NAME = "user-registry";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @Nullable final User user = getUserEndpoint()
                .registryUser(new UserRegistryRequest(login, password, email))
                .getUser();
        displayUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return null;
    }

}
