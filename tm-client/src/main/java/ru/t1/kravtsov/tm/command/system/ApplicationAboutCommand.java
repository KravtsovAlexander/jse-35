package ru.t1.kravtsov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Display developer info.";

    @NotNull
    public static final String NAME = "about";

    @Override
    public void execute() {
        @NotNull final IPropertyService propertyService = getPropertyService();
        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + propertyService.getApplicationName());
        System.out.println();

        System.out.println("[DEVELOPER]");
        System.out.println("NAME: " + propertyService.getAuthorName());
        System.out.println("EMAIL: " + propertyService.getAuthorEmail());
        System.out.println();

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + propertyService.getGitBranch());
        System.out.println("COMMIT ID: " + propertyService.getGitCommitId());
        System.out.println("COMMIT MESSAGE: " + propertyService.getGitCommitMessage());
        System.out.println("COMMIT TIME: " + propertyService.getGitCommitTime());
        System.out.println("COMMITTER NAME: " + propertyService.getGitCommitterName());
        System.out.println("COMMITTER EMAIL: " + propertyService.getGitCommitterEmail());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
